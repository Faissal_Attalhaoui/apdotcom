﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APDotCom
{
    class APDotCom
    {
        public static void Bestel()
        {
            Console.WriteLine("Prijs van een boek?");
            Console.Write("> ");
            double prijsBoek = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Prijs van een CD?");
            Console.Write("> ");
            double prijsCD = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Prijs van een servies?");
            Console.Write("> ");
            double prijsServies = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Prijs van een springkasteel?");
            Console.Write("> ");
            double prijsSpringkasteel = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Aantal boeken?");
            Console.Write("> ");
            int aantBoeken = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Aantal CD's?");
            Console.Write("> ");
            int aantCDs = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Aantal serviezen?");
            Console.Write("> ");
            int aantServiezen = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Aantal springkastelen");
            Console.Write("> ");
            int aantSpringkastelen = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Percentage korting?");
            Console.Write("> ");
            double korting = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Uw kasticket");
            Console.WriteLine("------------");

            double totBoek = prijsBoek * aantBoeken;
            double totCDs = prijsCD * aantCDs;
            double totServies = prijsServies * aantServiezen;
            double totSpringkasteel = prijsSpringkasteel * aantSpringkastelen;
            double totaal = totBoek + totCDs + totServies + totSpringkasteel;

            Console.WriteLine($"boek x {aantBoeken}: {totBoek:F2}");
            Console.WriteLine($"CD x {aantCDs}: {totCDs:F2}");
            Console.WriteLine($"servies x {aantServiezen}: {totServies:F2}");
            Console.WriteLine($"springkasteel x {aantSpringkastelen}: {totSpringkasteel:F2}");
            Console.WriteLine($"KORTING: {korting}%");
            Console.WriteLine($"TOTAAL VOOR KORTING: {totaal:F2}");
            Console.WriteLine($"TOTAAL: {totaal * (1 - korting / 100):F2}");
        }

        public static void BestelMetVraagEnAanbod()
        {
            Console.WriteLine("Basisprijs van een boek?");
            Console.Write("> ");
            double prijsBoek = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Basisprijs van een CD?");
            Console.Write("> ");
            double prijsCD = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Basisprijs van een servies?");
            Console.Write("> ");
            double prijsServies = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Basisprijs van een springkasteel?");
            Console.Write("> ");
            double prijsSpringkasteel = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Aantal boeken?");
            Console.Write("> ");
            int aantBoeken = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Aantal CD's?");
            Console.Write("> ");
            int aantCDs = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Aantal serviezen?");
            Console.Write("> ");
            int aantServiezen = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Aantal springkastelen");
            Console.Write("> ");
            int aantSpringkastelen = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Percentage korting?");
            Console.Write("> ");
            double korting = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Uw kasticket");
            Console.WriteLine("------------");

            double totBoek = prijsBoek * aantBoeken;
            double totCDs = prijsCD * aantCDs;
            double totServies = prijsServies * aantServiezen;
            double totSpringkasteel = prijsSpringkasteel * aantSpringkastelen;
            double totaal = totBoek + totCDs + totServies + totSpringkasteel;

            Random VraagAanbod = new Random();
            int VABoeken = VraagAanbod.Next(-50, 51);
            int VACDs = VraagAanbod.Next(-50, 51);
            int VAServiezen = VraagAanbod.Next(-50, 51);
            int VASpringkastelen = VraagAanbod.Next(-50, 51);

            Console.WriteLine($"vraag en aanbod boeken: {VABoeken}%");
            Console.WriteLine($"vraag en aanbod CD's: {VACDs}%");
            Console.WriteLine($"vraag en aanbod serviezen: {VAServiezen}%");
            Console.WriteLine($"vraag en aanbod springkastelen: {VASpringkastelen}%");

            Console.WriteLine($"boek x {aantBoeken}: {totBoek * (1 - (VABoeken / 100d)):F2}");
            Console.WriteLine($"CD x {aantCDs}: {totCDs * (1 - VACDs / 100d):F2}");
            Console.WriteLine($"servies x {aantServiezen}: {totServies * (1 - VASpringkastelen / 100d):F2}");
            Console.WriteLine($"springkasteel x {aantSpringkastelen}: {totSpringkasteel * (1 - VASpringkastelen / 100d):F2}");
            Console.WriteLine($"KORTING: {korting}%");
            Console.WriteLine($"TOTAAL VOOR KORTING: {totaal:F2}");
            Console.WriteLine($"TOTAAL: {totaal * (1 - korting / 100):F2}");
        }

        public static void BestelConditioneel()
        {
            Console.Write($"Prijs van een boek? \n>");
            double prijsBoek = Convert.ToDouble(Console.ReadLine());
            Console.Write($"Prijs van een CD? \n>");
            double prijsCD = Convert.ToDouble(Console.ReadLine());
            Console.Write($"Prijs van een servies? \n>");
            double prijsServies = Convert.ToDouble(Console.ReadLine());
            Console.Write($"Prijs van een sprinkasteel? \n>");
            double prijsSpringkasteel = Convert.ToDouble(Console.ReadLine());
            Console.Write($"Aantal boeken?\n>");
            int aantalBoek = Convert.ToInt32(Console.ReadLine());
            Console.Write($"Aantal CD's?\n>");
            int aantalCD = Convert.ToInt32(Console.ReadLine());
            Console.Write($"Aantal serviezen?\n>");
            int aantalServies = Convert.ToInt32(Console.ReadLine());
            Console.Write($"Aantal springkastelen?\n>");
            int aantalSpringkasteel = Convert.ToInt32(Console.ReadLine());
            Console.Write($"Percentage korting?\n>");
            int korting = Convert.ToInt32(Console.ReadLine());
            if (korting >= 30)
            {
                Console.WriteLine("Waarschuwing: het ingevoerde percentage is te hoog!");
            }
            Console.WriteLine("Worden prijsstijgingen en -dalingen toegepast? (J/N)");
            string toegepast = Console.ReadLine();
            if (toegepast == "J")
            {
                Random generator = new Random();
                int procentBoek = generator.Next(-50, 51);
                int procentCD = generator.Next(-50, 51);
                int procentServies = generator.Next(-50, 51);
                int procentSpringkasteel = generator.Next(-50, 51);

                double nieuwePrijsBoek = prijsBoek + prijsBoek * procentBoek / 100;
                double nieuwePrijsCD = prijsCD + prijsCD * procentBoek / 100;
                double nieuwePrijsServies = prijsServies + prijsServies * procentBoek / 100;
                double nieuwePrijsSpringkasteel = prijsSpringkasteel + prijsSpringkasteel * procentBoek / 100;

                Console.WriteLine($"Uw kasticket\n ------------");

                Console.Write("vraag en aanbod boeken: ");
                if (procentBoek < 0)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                }
                else if (procentBoek > 0)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                }
                Console.WriteLine($"{procentBoek}%");
                Console.ResetColor();

                Console.WriteLine($"vraag en aanbod CD's: {procentCD}%");
                Console.WriteLine($"vraag en aanbod serviezen: {procentServies}%");
                Console.WriteLine($"vraag en aanbod : {procentSpringkasteel}%");

                Console.WriteLine($"boek x {aantalBoek}: {nieuwePrijsBoek * aantalBoek:F2}");
                Console.WriteLine($"CD x {aantalCD}: {nieuwePrijsCD * aantalCD:F2}");
                Console.WriteLine($"servies x {aantalServies}: {nieuwePrijsServies * aantalServies:F2}");
                Console.WriteLine($"springkasteel x {aantalSpringkasteel}: {nieuwePrijsSpringkasteel * aantalSpringkasteel:F2}");

                double totaal = Convert.ToDouble((nieuwePrijsBoek * aantalBoek) + (nieuwePrijsCD * aantalCD) + (nieuwePrijsServies * aantalServies) + (nieuwePrijsSpringkasteel * aantalSpringkasteel));
                Console.WriteLine($"KORTING: {korting}%");
                Console.WriteLine($"TOTAAL VOOR KORTING: {totaal:F2}");
                double kortingsCalc2 = (totaal / 100) * korting;
                Console.WriteLine($"TOTAAL: {totaal - kortingsCalc2:F2}");
            }
            else if (toegepast == "N")
            {
                Console.WriteLine($"Uw kasticket\n ------------");
                Console.WriteLine($"boek x {aantalBoek}: {prijsBoek * aantalBoek:F2}");
                Console.WriteLine($"CD x {aantalCD}: {prijsCD * aantalCD:F2}");
                Console.WriteLine($"servies x {aantalServies}: {prijsServies * aantalServies:F2}");
                Console.WriteLine($"springkasteel x {aantalSpringkasteel}: {prijsSpringkasteel * aantalSpringkasteel:F2}");
                double totaal = Convert.ToDouble((prijsBoek * aantalBoek) + (prijsCD * aantalCD) + (prijsServies * aantalServies) + (prijsSpringkasteel * aantalSpringkasteel));
                Console.WriteLine($"KORTING: {korting}%");
                Console.WriteLine($"TOTAAL VOOR KORTING: {totaal:F2}");
                double kortingsCalc1 = (totaal / 100) * korting;
                Console.WriteLine($"TOTAAL: {totaal - kortingsCalc1:F2}");
            }
        }
    }
}
